package com.epam.controller;

public interface IFirstTaskController {
    void printAverage(int firstValue, int secondValue, int thirdValue);
    void printMaxValue(int firstValue, int secondValue, int thirdValue);
}

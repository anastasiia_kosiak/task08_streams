package com.epam.controller;

import java.util.List;

public interface IFourthTaskController {
    void printInformation(List<String> words);
}

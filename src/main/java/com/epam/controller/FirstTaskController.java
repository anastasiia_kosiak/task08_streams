package com.epam.controller;

import com.epam.model.FirstModel;
import com.epam.model.FirstTask;


public class FirstTaskController implements IFirstTaskController {
    private FirstModel model;

    public FirstTaskController() {
        model = new FirstTask();
    }
    @Override
    public void printAverage(int firstValue, int secondValue, int thirdValue) {
        model.printAverage(firstValue,secondValue,thirdValue);
    }

    @Override
    public void printMaxValue(int firstValue, int secondValue, int thirdValue) {
        model.printMaxValue(firstValue,secondValue,thirdValue);
    }
};

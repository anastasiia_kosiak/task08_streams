package com.epam.controller;

import com.epam.model.ThirdTask;
import com.epam.model.ThirdModel;

public class ThirdTaskController implements IThirdTaskController {
    private ThirdModel model;

    public ThirdTaskController() {
        model = new ThirdTask();
    }
    @Override
    public void printStatistics() {
        model.printStatistics();
    }
}

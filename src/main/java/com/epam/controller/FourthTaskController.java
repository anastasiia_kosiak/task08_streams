package com.epam.controller;

import com.epam.model.FourthModel;
import com.epam.model.FourthTask;

import java.util.List;

public class FourthTaskController implements IFourthTaskController {
    private FourthModel model;

    public FourthTaskController() {
        model = new FourthTask();
    }

    @Override
    public void printInformation(List<String> words) {
        model.printInformation(words);
    }
}

package com.epam.command;
@FunctionalInterface
public interface Command {
    void execute();
}

package com.epam.command;

public class Cat {
    private String name;
    private int age;
    private String breed;

    public Cat(String name, int age, String breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void feed(){
        System.out.println("I'm huungry!!!");
    }

    public void makeSound(){
        System.out.println("Meow-meow");
    }

    public void sleep(){
        System.out.println("...");
    }

    public void play(){
        System.out.println("I want you to play with me, human");
    }
}

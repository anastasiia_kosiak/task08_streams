package com.epam.command;

class Owner {
    private Command feed;
    private Command play;
    private Command makeSound;
    private Command sleep;

    public Owner(Command feed, Command play, Command makeSound, Command sleep) {
        this.feed = feed;
        this.play = play;
        this.makeSound = makeSound;
        this.sleep = sleep;
    }

    void feedCat(){
        feed.execute();
    }

    void playWithACat(){
        play.execute();
    }

    void sleep(){
        sleep.execute();
    }
    void makeSound(){
        makeSound.execute();
    }
}

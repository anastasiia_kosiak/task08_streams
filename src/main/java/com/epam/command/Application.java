package com.epam.command;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Cat Schwartz = new Cat("Schwartz", 6, "Scottish fold");
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("1 -> Feed a cat\n2 -> Play with a cat\n" +
                    "3 -> Let the cat sleep\n4 -> Let the cat make a sound\n");
            System.out.println("Input name of command: ");
            String chooseMethod = sc.nextLine();

            Owner me = new Owner(
                    () -> Schwartz.feed(),
                    Schwartz::sleep,
                    new Command() {
                        @Override
                        public void execute() {
                            Schwartz.makeSound();
                        }
                    },
                    new PlayCommand());
            switch (chooseMethod) {
                case "1":
                    me.feedCat();
                    break;
                case "2":
                    me.playWithACat();
                    break;
                case "3":
                    me.sleep();
                    break;
                case "4":
                    me.makeSound();
                    break;
            }

        }
    }
}

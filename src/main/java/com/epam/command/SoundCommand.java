package com.epam.command;

public class SoundCommand implements Command {
    private Cat cat;
    @Override
    public void execute(){
        cat.makeSound();
    }
}

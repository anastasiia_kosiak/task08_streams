package com.epam.command;

public class SleepCommand implements Command{
    private Cat cat;

    @Override
    public void execute(){
        cat.sleep();
    }
}

package com.epam.command;

public class FeedCommand implements Command {
    private Cat cat;
    @Override
    public void execute(){
        cat.feed();
    }
}

package com.epam.command;

public class PlayCommand implements Command{
    private Cat cat;
    @Override
    public void execute(){
        cat.play();
    }
}

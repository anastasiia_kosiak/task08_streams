package com.epam.model;

@FunctionalInterface
public interface Test {
    int method(int firstValue, int secondValue, int thirdValue);
}

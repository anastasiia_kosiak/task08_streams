package com.epam.model;

import java.util.List;

public class FourthTask implements FourthModel {
    private FourthTaskFunctionality functionality;

    public FourthTask() {
        functionality = new FourthTaskFunctionality();
    }

    @Override
    public void printInformation(List<String> words) {
        functionality.printInformation(words);
    }
};
package com.epam.model;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FourthTaskFunctionality {
    private long getNumberOfUniqueWords(List<String> words) {
        return words.stream().distinct().count();
    }
    private List<String> getSortedUnique(List<String> words) {
        return words.stream()
                .distinct().sorted().collect(Collectors.toList());
    }

    private int getNumberOfTextLines(List<String> words) {
        return words.size();
    }

    private Map<String, Long> getNumberOfEachWord(List<String> words) {
        return words.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.groupingBy(value -> value, Collectors.counting()));
    }

    private Map<String, Long> getNumOfEachCharWithoutUpperCase(List<String> words) {
        return words.stream()
                .flatMap(e -> Stream.of(e.split("")))
                .flatMap(s -> Stream.of(s.split("")))
                .filter(a -> !(a.equals(a.toUpperCase())))
                .collect(Collectors.groupingBy(value -> value, Collectors.counting()));
    }
    public void printInformation(List<String> words) {
        System.out.println();
        System.out.println("Number of text lines: " + getNumberOfTextLines(words));
        System.out.println("Number of unique words: " + getNumberOfUniqueWords(words));
        System.out.println("Sorted unique words : ");
        System.out.println(getSortedUnique(words));
        System.out.println("Number of each word: \n" + getNumberOfEachWord(words));
        System.out.println("Number of each without upper case characters :");
        System.out.println(getNumOfEachCharWithoutUpperCase(words));
    }
}

package com.epam.model;

public class ThirdTask implements ThirdModel {
    private ThirdTaskFunctionality functionality;

    public ThirdTask() {
        functionality = new ThirdTaskFunctionality();
    }

    @Override
    public void printStatistics() {
        functionality.printStats();
    }
}

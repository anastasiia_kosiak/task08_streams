package com.epam.model;

public class FirstTask implements FirstModel{
    private FirstTaskFunctionality functionality;

    public FirstTask() {
        functionality = new FirstTaskFunctionality();
    }

    @Override
    public void printAverage(int firstValue, int secondValue, int thirdValue) {
        functionality.printAverage(firstValue,secondValue,thirdValue);
    }

    @Override
    public void printMaxValue(int firstValue, int secondValue, int thirdValue) {
        functionality.printMaxValue(firstValue,secondValue,thirdValue);
    }
}

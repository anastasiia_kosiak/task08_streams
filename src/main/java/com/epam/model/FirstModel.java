package com.epam.model;

public interface FirstModel {
    void printAverage(int firstValue, int secondValue, int thirdValue);
    void printMaxValue(int firstValue, int secondValue, int thirdValue);
}

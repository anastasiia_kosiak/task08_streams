package com.epam.model;

import java.util.Arrays;
import java.util.Random;
import java.util.IntSummaryStatistics;
import java.util.List;

public class ThirdTaskFunctionality {
    private final static Random random = new Random();

    private int getRandomIntegerInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Maximum value must be greater than minimum");
        }
        return random.nextInt((max - min) + 1) + min;
    }

    private long countValuesBiggerThanAverage() {
        Integer[] array = getArray();
        System.out.println(Arrays.toString(array));
        double average = Arrays.stream(array).mapToDouble(value -> value).average().orElse(0);
        System.out.println("Average number: " + average);
        return Arrays.stream(array).filter(value -> value > average).count();
    }

    void printStats() {
        System.out.println(getStatistics());
        System.out.println();
        System.out.println("The number of values bigger than average: " + countValuesBiggerThanAverage());
    }

    private IntSummaryStatistics getStatistics() {
        List<Integer> list = Arrays.asList(getArray());
        System.out.println(list);
        return list.stream()
                .mapToInt((value) -> value)
                .summaryStatistics();
    }

    private Integer[] getArray() {
        int size = 1 + random.nextInt(10);
        Integer[] array = new Integer[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = getRandomIntegerInRange(5, 100);
        }
        return array;
    }
}

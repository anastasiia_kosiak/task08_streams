package com.epam.model;

import java.util.stream.Stream;

public class FirstTaskFunctionality {
    void printAverage(int firstValue, int secondValue, int thirdValue) {
        Test averageValue = (a, b, c) -> (a + b + c) / 3;
        System.out.println(averageValue.method(firstValue, secondValue, thirdValue));
    }

    void printMaxValue(int firstValue, int secondValue, int thirdValue) {
        Integer maxValue = Stream.of(firstValue, secondValue, thirdValue).mapToInt(v -> v).max().orElse(0);
        System.out.println(maxValue);
    }
}

package com.epam.model;

import java.util.List;

public interface FourthModel {
    void printInformation(List<String> words);
}

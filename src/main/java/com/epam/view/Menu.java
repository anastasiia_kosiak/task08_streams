package com.epam.view;
import com.epam.controller.*;
import java.util.*;

public class Menu {
    private IFirstTaskController first;
    private IThirdTaskController third;
    private IFourthTaskController fourth;
    private Map<String, String> menu;
    private Map<String, Printable> methods;
    private static Scanner input = new Scanner(System.in);

    public Menu() {
        first = new FirstTaskController();
        third = new ThirdTaskController();
        fourth = new FourthTaskController();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 -> Find average value");
        menu.put("2", "  2 -> Find max value");
        menu.put("3", "  3 -> Print states");
        menu.put("4", "  4 -> Information about words");
        menu.put("X", "  X -> exit");

        methods = new LinkedHashMap<>();
        methods.put("1", this::func1);
        methods.put("2", this::func2);
        methods.put("3", this::func3);
        methods.put("4", this::func4);
    }

    private void func1() {
        System.out.println("Input first number: ");
        int firstNumber = input.nextInt();
        System.out.println("Input second number: ");
        int secondNumber = input.nextInt();
        System.out.println("Input third number: ");
        int thirdNumber = input.nextInt();
        System.out.print("Average value: ");
        first.printAverage(firstNumber, secondNumber, thirdNumber);
    }

    private void func2() {
        System.out.println("Input first number: ");
        int firstNumber = input.nextInt();
        System.out.println("Input second number: ");
        int secondNumber = input.nextInt();
        System.out.println("Input third number: ");
        int thirdNumber = input.nextInt();
        System.out.println("Max value: ");
        first.printMaxValue(firstNumber, secondNumber, thirdNumber);
    }

    private void func3() {
        third.printStatistics();
    }

    private void func4() {
        System.out.println();
        List<String> words = new ArrayList<>();
        String string;
        do {
            System.out.println("Input word: ");
            string = input.nextLine();
            if (!string.equals("")) {
                words.add(string);
            }
        } while (!string.equals(""));
        System.out.println("Initial words list \n" + words);
        fourth.printInformation(words);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Choose a menu item");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methods.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("X"));
    }

}
